﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunestoneActivator : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            gameObject.transform.parent.GetComponent<RunestoneSound>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            gameObject.transform.parent.GetComponent<RunestoneSound>().enabled = false;
        }
    }
}
