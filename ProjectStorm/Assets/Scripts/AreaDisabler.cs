﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaDisabler : MonoBehaviour {

    public AudioSource ambience_source;

    public AudioClip firstAmbience;
    public AudioClip secondAmbience;

    public GameObject first_area;
    public GameObject second_area;

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            switch (first_area.activeInHierarchy) {
                case true:
                    first_area.SetActive(false);
                    second_area.SetActive(true);
                    ambience_source.clip = secondAmbience;
                    ambience_source.Play();
                    break;
                case false:
                    second_area.SetActive(false);
                    first_area.SetActive(true);
                    ambience_source.clip = firstAmbience;
                    ambience_source.Play();
                    break;
                default:
                    Debug.LogError("Wait, how did a bool become neither true nor false..?");
                    first_area.SetActive(true);
                    second_area.SetActive(true);
                    ambience_source.clip = secondAmbience;
                    ambience_source.Play();
                    break;
            }
        }
    }
}
