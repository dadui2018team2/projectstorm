﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioBank : MonoBehaviour{

    public AudioMixer masterMixer;

    [Header("Player sounds")]
    public AudioClip[] player_footsteps;
    public AudioClip[] player_death;
    public AudioClip player_beserk;

    [Header("Enemy sounds")]
    public AudioClip[] enemy_footsteps;
    public AudioClip[] enemy_heard;
    public AudioClip[] enemy_seen;
    public AudioClip[] enemy_death;

    [Header("Pick-up sounds")]
    public AudioClip runestone_emitting;
    public AudioClip runestone_pickUp;
    public AudioClip checkpoint_reached;

    private AudioSource audioSource;

    public Slider musicSlider;
    public Slider soundSlider;

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        if (PlayerPrefs.HasKey("Music_Volume")) {
            musicSlider.value = PlayerPrefs.GetFloat("Music_Volume");
            masterMixer.SetFloat("musicVol", musicSlider.value);
        }
        if (PlayerPrefs.HasKey("Sound_Volume")) {
            soundSlider.value = PlayerPrefs.GetFloat("Sound_Volume");
            masterMixer.SetFloat("sfxVol", soundSlider.value);
        }
    }

    public void PlayButtonClick() {
        audioSource.Play();
    }

    public void SetMusicVolume(float value) {
        masterMixer.SetFloat("musicVol", value);
        PlayerPrefs.SetFloat("Music_Volume", value);
    }

    public void SetSFXVolume(float value) {
        masterMixer.SetFloat("sfxVol", value);
        PlayerPrefs.SetFloat("Sound_Volume", value);
    }
}
