﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    public AudioSource audioSource;
    private int footstepMaxIndex;
    private int deathSoundMaxIndex;

    public float[] footstepInterval;
    public float currentCounter;

    public bool isRunning;

    public string zone;

    private void Start() {
        if (audioSource == null)
            audioSource = GetComponentInChildren<AudioSource>();

        if (gameObject.tag == "Player") {
            footstepMaxIndex = GameManager.GetAudioBank.player_footsteps.Length;
            deathSoundMaxIndex = GameManager.GetAudioBank.player_death.Length;
        }
        else if (gameObject.tag == "Enemy") {
            footstepMaxIndex = GameManager.GetAudioBank.enemy_footsteps.Length;
            deathSoundMaxIndex = GameManager.GetAudioBank.enemy_death.Length;
        }

        if (footstepInterval.Length > 0) {
            currentCounter = footstepInterval[0];
        }
        isRunning = false;
    }

    public void PlayFootsteps(string eventName) {
        currentCounter -= Time.deltaTime;

        if (currentCounter <= 0) {
            if (audioSource.isPlaying)
                return;

            //AkSoundEngine.PostEvent(eventName, gameObject);
            int random = Random.Range(0, footstepMaxIndex);

            if(gameObject.tag == "Player")
                audioSource.clip = GameManager.GetAudioBank.player_footsteps[random];
            else if(gameObject.tag == "Enemy")
                audioSource.clip = GameManager.GetAudioBank.enemy_footsteps[random];

            audioSource.Play();

            if (isRunning) {
                currentCounter = footstepInterval[1];
            }
            else {
                currentCounter = footstepInterval[0];
            }
        }
    }

    public void PlayDeathSound() {
        int random = Random.Range(0, deathSoundMaxIndex);

        if (gameObject.tag == "Player")
            audioSource.clip = GameManager.GetAudioBank.player_death[random];
        else if (gameObject.tag == "Enemy")
            audioSource.clip = GameManager.GetAudioBank.enemy_death[random];

        audioSource.Play();
    }

    public void SetSprint(bool val) {
        isRunning = val;
    }
}
