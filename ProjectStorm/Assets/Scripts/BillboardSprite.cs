﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardSprite : MonoBehaviour {

    Transform cameraTransform;

    private void Start() {
        cameraTransform = Camera.main.transform;
    }


    void Update () {
        transform.forward = cameraTransform.forward;
	}
}
