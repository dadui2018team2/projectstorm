﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Checkpoint : MonoBehaviour {

    private AudioSource audioSource;

	public GameObject spawnObject;
    public GameObject fire;

	private int index;
    private ResetLevel resetLevelScript;
	private Vector3 spawnPoint;

	// Use this for initialization
	void Start () {
        resetLevelScript = FindObjectOfType<ResetLevel>();
		if (spawnObject != null) spawnPoint = spawnObject.transform.position;
		else spawnPoint = transform.position + transform.forward;

	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag.Equals("Player")){
			if (index > GameManager.GetCheckpointManager.GetCurrentCheckpoint.index){
                if(!fire.activeSelf && index != 0) fire.SetActive(true);
                GameManager.GetRuneManager.DisableRuneUI();
                GameManager.GetRuneManager.UpdateRuneList();
                GameManager.GetRuneManager.PrintRuneList();
                GameManager.GetRuneManager.DisablePreviousRunes();
				// Activate checkpoint
				GameManager.GetCheckpointManager.NewCheckpoint(this);
            	Debug.Log(index);

                SaveCheckPoint(SceneManager.GetActiveScene().name);

                if (audioSource == null)
                    audioSource = GetComponent<AudioSource>();

                if (audioSource.clip == null)
                    audioSource.clip = GameManager.GetAudioBank.checkpoint_reached;

                audioSource.Play();

                //AkSoundEngine.PostEvent("Play_CheckPoint1", gameObject);
                resetLevelScript.SetIndex = index;
			}
		}
	}

    private void SaveCheckPoint(string sceneName) {
        string key = sceneName + "_checkpoint";
        PlayerPrefs.SetInt(key, index);
    }

	public int SetIndex{
		set{index = value;}
	}

    public int GetIndex{
        get {return index;}
    }

	public Vector3 GetSpawnPoint{
		get { return spawnObject.transform.position; }
	}
}
