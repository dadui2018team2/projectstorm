﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointAreaDisabler : MonoBehaviour {

    public GameObject myArea;

    public GameObject[] otherAreas = new GameObject[2];

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            myArea.SetActive(true);

            foreach (GameObject obj in otherAreas)
                obj.SetActive(false);
        }
    }
}
