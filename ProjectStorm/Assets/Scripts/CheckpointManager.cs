﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckpointManager : MonoBehaviour {

	public Checkpoint[] checkpoints;

	private Checkpoint currentCheckpoint;
	private string startpointKey = "Checkpoint";

	void Start () {
		InitializeCheckpoints();
	}

	public void InitializeCheckpoints(){
		if (checkpoints.Length > 0){
			int startpoint = PlayerPrefs.GetInt(startpointKey, 0);
			if (startpoint > checkpoints.Length){
				startpoint = 0;
			} 
			currentCheckpoint = checkpoints[startpoint];
			for (int i = 0; i < checkpoints.Length; i++){
				checkpoints[i].SetIndex = i;
			}

			GameManager.PlayerToCheckpoint(currentCheckpoint.GetSpawnPoint);
		}
	}

	public void NewCheckpoint(Checkpoint newPoint){
		currentCheckpoint = newPoint;
        int savedPoint = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "_checkpoint", 0);
        if (currentCheckpoint.GetIndex > savedPoint)
        {
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name +"_checkpoint", currentCheckpoint.GetIndex);
        }
    }

    public Checkpoint GetCurrentCheckpoint
    {
        get { return currentCheckpoint; }
    }
}
