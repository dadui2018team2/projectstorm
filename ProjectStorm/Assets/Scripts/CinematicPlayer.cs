﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CinematicPlayer : MonoBehaviour {

    [Tooltip("The scene that should automatically be loaded when the cinematic is finished playing. Should be the same as in the 'Skip'-button.")]
    public string nextScene;

    [Tooltip("The background image that should always be at the top in the hierarchy.")]
    public Image backgroundImage;

    [Tooltip("The FIRST image component that will be showing cinematics.")]
    public Image cinematicImage1;

    [Tooltip("The SECOND image component that will be showing cinematics.")]
    public Image cinematicImage2;

    [Tooltip("Insert sprites in the order they should be played. OBS: First must be BlackScreen.png, and last TWO must be BlackScreen.png")]
    public List<Sprite> sprites = new List<Sprite>();

    [Header("Timing settings")]
    public float timePerImage = 5f;
    public float lerpSteps = 0.02f;

    public AudioSource cinematicSounds;


    private float timeLeft = 0f;
    private Image activeImage;
    private  Image inactiveImage;
    private Coroutine fader = null;

    private void Start() {
        activeImage = cinematicImage1;
        inactiveImage = cinematicImage2;

        AssignNewSpriteTo(activeImage);
        AssignNewSpriteTo(inactiveImage);
    }

    private void Update() {
        if (sprites.Count == 0)
            FinishCinematic();

        if (timeLeft <= 0) {
            if (fader == null) {
                fader = StartCoroutine("Fade");
            }
        }
        else {
            timeLeft -= Time.deltaTime;

            if (sprites.Count == 0 && timeLeft <= 0) {
                FinishCinematic();
                return;
            }
        }
    }

    private IEnumerator Fade() {
        Color transparent = new Color(1f, 1f, 1f, 0f);
        float lerpValue = 0f;
        backgroundImage.sprite = inactiveImage.sprite;

        if (cinematicSounds.isPlaying == false)
            cinematicSounds.Play();

        while (lerpValue < 1f) {
            lerpValue += lerpSteps;
            activeImage.color = Color.Lerp(Color.white, transparent, lerpValue);
            inactiveImage.color = Color.Lerp(transparent, Color.white, lerpValue);

            yield return new WaitForEndOfFrame();
        }

        SwitchImages();
        timeLeft = timePerImage;
        fader = null;
    }

    private void AssignNewSpriteTo(Image img) {
        if (sprites.Count > 0) {
            img.sprite = sprites[0];
            sprites.RemoveAt(0);
        }
    }

    private void SwitchImages() {
        if (activeImage == cinematicImage1) {
            activeImage = cinematicImage2;
            inactiveImage = cinematicImage1;
        }
        else {
            activeImage = cinematicImage1;
            inactiveImage = cinematicImage2;
        }

        activeImage.color = Color.white;
        inactiveImage.color = Color.clear;

        AssignNewSpriteTo(inactiveImage);
    }

    private void FinishCinematic() {
        SceneManager.LoadScene(nextScene);
    }
}
