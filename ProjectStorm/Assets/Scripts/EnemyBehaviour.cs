﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour {

    public List<Transform> patrolPositions;

    private AudioSource audioSource;

    AudioClip[] deathSounds = new AudioClip[0];
    AudioClip[] heardSounds = new AudioClip[0];
    AudioClip[] seenSounds = new AudioClip[0];

    public float killDistance;
    public float patrolSpeed;
    public float chaseSpeed;

    [Header ("Patrolling")]
    public float rotationSpeed;
    public float rotationAngle;
    public float waitTime;

    [Header ("Player Movement Detection")]
    public float walkDistance;
    public float sprintDistance;

    struct TransformSave{
        public Vector3 pos;
        public Quaternion rot;

        public Transform Set{
            set { 
                pos = value.position;
                rot = value.rotation;
            }
        }
    }
    private TransformSave detectPoint;

    private NavMeshAgent navAgent;
    private TransformSave stationPoint;
    private float timer = 0;

    // For reset
    private Vector3 initPos;
    private Quaternion initRot;
    private List<Transform> initPatrolPositions;

    private float soundTimer = 0;

    private enum PatrolState
    {
        Stationed,
        Patrol,
        LookForward,
        LookLeft,
        LookRight,
        LookNext,
        Chase,
        Investigate,
        Dead
    }

    private PatrolState currentState;
    private PatrolState prevState;
    private PatrolState initState;

    private Animator animator;
    public GameObject lightCone;
    private CapsuleCollider capsule;
    private SkinnedMeshRenderer meshRenderer;
    private FieldOfView view;

    public AudioController ac;
    public bool playFootsteps;
    public float footstepTimer;
    

    private void Awake() {
        audioSource = GetComponent<AudioSource>();
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        capsule = GetComponent<CapsuleCollider>();
        meshRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
        view = GetComponent<FieldOfView>();
        initPos = navAgent.destination;
        initRot = transform.rotation;
        initPatrolPositions = patrolPositions;
    }

    private void Start() {
        if (patrolPositions.Count > 1){
            initState = PatrolState.Patrol;
            currentState = initState;
        }
        else {
            initState = PatrolState.Stationed;
            currentState = initState;
            if (patrolPositions[0] != null){
                stationPoint.Set = patrolPositions[0];
            }
            else{
                stationPoint.Set = transform;
            }
        }

        ac = gameObject.GetComponent<AudioController>();
        footstepTimer = Time.time;
    }

    public void ResetEnemy(){
        if (navAgent.hasPath) navAgent.ResetPath();
        navAgent.Warp(initPos);
        transform.rotation = initRot;
        currentState = initState;
        patrolPositions = initPatrolPositions;
        timer = 0;
        lightCone.SetActive(true);
        capsule.enabled = true;
        meshRenderer.enabled = true;
        Color c = meshRenderer.material.color;
        c.a = 1;
        meshRenderer.material.color = c;
        view.visibleTargets.Clear();
        animator.SetTrigger("Restart");
    }

    public void Disarm(){
        lightCone.SetActive(false);
    }

    private void Update() {
        // If dead, don't continue from here
        if (currentState == PatrolState.Dead) return;

        animator.SetFloat("Speed", navAgent.velocity.magnitude);
        if (GameManager.IsPlayerOnShrooms) animator.SetBool("Back", true);
        else animator.SetBool("Back", false);
        if (currentState == PatrolState.Chase) animator.SetBool("Chase", true);
        else animator.SetBool("Chase", false);
        if (GameManager.GetPlayer.canMove) navAgent.isStopped = false;

        if (PlayerInView() && GameManager.GetPlayer.canMove){
            if (currentState != PatrolState.Chase){
                //AkSoundEngine.PostEvent("Play_AlertSounds_Seen", gameObject);

                if (seenSounds.Length < 1)
                    seenSounds = GameManager.GetAudioBank.enemy_seen;

                int random = Random.Range(0, seenSounds.Length);
                audioSource.clip = seenSounds[random];
                audioSource.Play();

                prevState = currentState;
                currentState = PatrolState.Chase;
            }
            timer = 0;
            MoveDetectPoint();
        }
        else {
            navAgent.speed = patrolSpeed;
        }

        if (timer < Time.time && GameManager.GetPlayer.canMove){
            PerformState();
        }

        if(navAgent.velocity.magnitude > 0 && playFootsteps) {

            //if (footstepTimer <= Time.time) {
            //    AkSoundEngine.PostEvent("Play_EnemyFootstepsWalk", gameObject);
            //    footstepTimer = Time.time + 2;
            //}

            ac.PlayFootsteps("Play_EnemyFootstepsWalk");
            if (currentState == PatrolState.Chase) {
                ac.SetSprint(true);
            }
            else {
                ac.SetSprint(false);
            }
        }
    }

    private void FixedUpdate() {

        if (currentState == PatrolState.Chase || currentState == PatrolState.Dead) return; // Are we already chasing?

        float distanceToPlayer = Vector3.Distance(transform.position, GameManager.GetPlayer.transform.position);
        float playerSpeed = GameManager.GetPlayer.CurrentSpeed;
        bool sprinting = GameManager.GetPlayer.Sprinting;
        if (playerSpeed > 0 && !sprinting){
            if (distanceToPlayer <= walkDistance){
                // Detect walk
                DetectPlayer();
            }
        }
        else if (playerSpeed > 0 && sprinting){
            if (distanceToPlayer <= sprintDistance){
                // Detect sprint
                DetectPlayer();
            }
        }
    }

    private void MoveDetectPoint(){
        detectPoint.Set = GameManager.GetPlayer.transform;
    }

    private void DetectPlayer(){
        if (currentState != PatrolState.Chase && soundTimer < Time.time){
            //AkSoundEngine.PostEvent("Play_AlertSounds_Heard", gameObject);

            if (heardSounds.Length < 1)
                heardSounds = GameManager.GetAudioBank.enemy_heard;

            int random = Random.Range(0, heardSounds.Length);
            audioSource.clip = heardSounds[random];
            audioSource.Play();

            soundTimer = Time.time + 2;
        }
        timer = 0;
        prevState = currentState;
        currentState = PatrolState.Investigate;
        MoveDetectPoint();
    }

    private void NextDestination(){
        patrolPositions.Add(patrolPositions[0]);
        patrolPositions.RemoveAt(0);
    }

    public void EnemyDeath(){
        if (currentState != PatrolState.Dead){
            animator.SetTrigger("Death");
            currentState = PatrolState.Dead;
            lightCone.SetActive(false);
            capsule.enabled = false;
            Invoke("HideEnemy", 4);

            if(deathSounds.Length < 1)
                deathSounds = GameManager.GetAudioBank.enemy_death;

            int random = Random.Range(0, deathSounds.Length);
            audioSource.clip = deathSounds[random];
            audioSource.Play();

            //AkSoundEngine.PostEvent("Play_EnemyDeathSound", gameObject);
        }
    }

    private void HideEnemy(){
        meshRenderer.enabled = false;
    }

    private void PerformState(){

        float distanceToPoint = 10f;
        NavMeshHit navMeshHit;
        if (NavMesh.SamplePosition(patrolPositions[0].position, out navMeshHit, distanceToPoint, NavMesh.AllAreas))
        {
            distanceToPoint =  Vector3.Distance(navAgent.destination, navMeshHit.position);
        }

        switch(currentState){
            case PatrolState.Stationed:
                if (NavMesh.SamplePosition(stationPoint.pos, out navMeshHit, distanceToPoint, NavMesh.AllAreas))
                {
                    distanceToPoint =  Vector3.Distance(navAgent.destination, navMeshHit.position);
                }

                if (!navAgent.pathPending && distanceToPoint > navAgent.stoppingDistance || prevState == PatrolState.Investigate){
                    if (prevState == PatrolState.Investigate) prevState = initState;
                    navAgent.destination = stationPoint.pos;
                }
                else if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance){
                    prevState = currentState;
                    currentState = PatrolState.LookForward;
                    break;
                }
                break;

            case PatrolState.Patrol:
                navAgent.speed = patrolSpeed;
                if (navAgent.velocity.magnitude == 0){
                    if (!navAgent.pathPending && distanceToPoint > navAgent.stoppingDistance){
                        navAgent.destination = patrolPositions[0].position;
                    }
                    else if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance){
                        prevState = currentState;
                        currentState = PatrolState.LookForward;
                        break;
                    }
                }
                break;

            case PatrolState.LookForward:
                Quaternion forwardRotation;
                if (prevState == PatrolState.Investigate) forwardRotation = Quaternion.LookRotation(detectPoint.rot * Vector3.forward, Vector3.up);
                else forwardRotation = Quaternion.LookRotation(patrolPositions[0].forward, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, forwardRotation, rotationSpeed * Time.deltaTime);
                if (transform.rotation == forwardRotation){
                    if (prevState == PatrolState.Investigate){
                        currentState = PatrolState.LookLeft;
                        timer = waitTime + Time.time;
                    }
                    else if (prevState == PatrolState.LookLeft){
                        prevState = currentState;
                        currentState = PatrolState.LookRight;
                    }
                    else {
                        prevState = currentState;
                        currentState = PatrolState.LookLeft;
                        timer = waitTime + Time.time;
                    }
                }
                break;

            case PatrolState.LookLeft:
                Quaternion leftRotation;
                if (prevState == PatrolState.Investigate) leftRotation = Quaternion.LookRotation(detectPoint.rot * -Vector3.right, Vector3.up);
                else leftRotation = Quaternion.LookRotation(-patrolPositions[0].right, Vector3.up);
                leftRotation = Quaternion.RotateTowards(patrolPositions[0].rotation, leftRotation, rotationAngle);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, leftRotation, rotationSpeed * Time.deltaTime);
                if (transform.rotation == leftRotation){
                    if (prevState == PatrolState.Investigate){
                        currentState = PatrolState.LookRight;
                        timer = waitTime + Time.time;
                    }
                    else {
                        prevState = currentState;
                        currentState = PatrolState.LookForward;
                        timer = waitTime + Time.time;
                    }
                }
                
                break;

            case PatrolState.LookRight:
                Quaternion rightRotation;
                if (prevState == PatrolState.Investigate) rightRotation = Quaternion.LookRotation(detectPoint.rot * Vector3.right, Vector3.up);
                else rightRotation = Quaternion.LookRotation(patrolPositions[0].right, Vector3.up);
                rightRotation = Quaternion.RotateTowards(patrolPositions[0].rotation, rightRotation, rotationAngle);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, rightRotation, rotationSpeed * Time.deltaTime);
                if (transform.rotation == rightRotation){
                    if (prevState == PatrolState.Investigate){
                        prevState = currentState;
                        currentState = initState;
                        timer = waitTime + Time.time;
                        break;
                    }
                    else if (initState == PatrolState.Stationed){
                        prevState = currentState;
                        currentState = PatrolState.LookForward;
                    }
                    else {
                        prevState = currentState;
                        currentState = PatrolState.LookNext;
                    }
                    timer = waitTime + Time.time;
                    
                }
                
                break;

            case PatrolState.LookNext:
                Quaternion nextRotation = Quaternion.LookRotation(patrolPositions[1].position - patrolPositions[0].position, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, nextRotation, rotationSpeed * Time.deltaTime);
                if (transform.rotation == nextRotation){
                    NextDestination();
                    prevState = currentState;
                    currentState = initState;
                    timer = waitTime + Time.time;
                }
                
                break;

            case PatrolState.Chase:
                if (GameManager.IsPlayerOnShrooms){
                    navAgent.destination = transform.position;
                    Quaternion lookPlayer = Quaternion.LookRotation(GameManager.GetPlayer.transform.position - transform.position, Vector3.up);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, lookPlayer, rotationSpeed * Time.deltaTime);
                    transform.position += -transform.forward * Time.deltaTime;
                    break;
                }
                navAgent.speed = chaseSpeed;
                if (PlayerInView()){
                    Vector3 expectedPos = GameManager.GetPlayer.transform.position + GameManager.GetPlayer.transform.forward * GameManager.GetPlayer.CurrentSpeed  * Time.deltaTime;
                    navAgent.destination = expectedPos;
                    if (Vector3.Distance(transform.position, GameManager.GetPlayer.transform.position) < killDistance){
                        animator.SetTrigger("Attack");
                        navAgent.isStopped = true;
                        navAgent.velocity = Vector3.zero;
                        GameManager.PlayerDeath();
                        currentState = initState;
                        timer = Time.time + 10;
                        GameManager.GetPlayer.canMove = false;
                    }
                }
                else {
                    currentState = PatrolState.Investigate;
                    MoveDetectPoint();
                    break;
                }
                break;
            
            case PatrolState.Investigate:
                // Go to point

                if (navAgent.velocity.magnitude == 0 || navAgent.destination != detectPoint.pos){
                    distanceToPoint =  Vector3.Distance(transform.position, detectPoint.pos);
                    if (NavMesh.SamplePosition(detectPoint.pos, out navMeshHit, distanceToPoint, NavMesh.AllAreas)){
                        distanceToPoint =  Vector3.Distance(navAgent.destination, navMeshHit.position);
                    }
                    if (!navAgent.pathPending && distanceToPoint > navAgent.stoppingDistance){
                        navAgent.destination = detectPoint.pos;
                    }
                    if (!navAgent.pathPending && navAgent.remainingDistance <= (navAgent.stoppingDistance + 0.1f)){
                        prevState = currentState;
                        currentState = PatrolState.LookForward;
                        timer = waitTime + Time.time;
                        break;
                    }
                } else {
                    prevState = currentState;
                    currentState = PatrolState.LookForward;
                    timer = waitTime + Time.time;
                }
                break;
        }
    }

    public void SetPlayFootsteps(bool val) {
        playFootsteps = val;
    }

    private bool PlayerInView(){
        List<Transform> seen = view.visibleTargets;
        if (seen == null) return false;

        for (int i = 0; i < seen.Count; i++){
            if (seen[i].tag.Equals("Player")) return true;
        }
        return false;
    }

}