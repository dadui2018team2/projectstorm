﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFootstepActivator : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            gameObject.transform.parent.GetComponent<EnemyBehaviour>().SetPlayFootsteps(true);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            gameObject.transform.parent.GetComponent<EnemyBehaviour>().SetPlayFootsteps(false);
        }
    }
}
