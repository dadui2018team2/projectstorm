﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyReset : MonoBehaviour {

	private EnemyBehaviour[] AllEnemies;

    private bool enemiesDisabled = false;

    public void FindAll(){
		AllEnemies = FindObjectsOfType<EnemyBehaviour>();
	}
	
	public void ResetAllEnemies(){
		if (AllEnemies != null){
			for (int i = 0; i < AllEnemies.Length; i++){
				AllEnemies[i].gameObject.SetActive(true);
				AllEnemies[i].ResetEnemy();
			}
		}
	}

    public void DisableEnemies(Button resetButton) {
        foreach (EnemyBehaviour obj in AllEnemies) {
            if (!enemiesDisabled)
                obj.gameObject.SetActive(false);
            else if (enemiesDisabled)
                obj.gameObject.SetActive(true);
        }

        if (!enemiesDisabled) {
            enemiesDisabled = true;
            resetButton.GetComponentInChildren<Text>().text = "Enable enemies";

        }
        else if (enemiesDisabled) {
            enemiesDisabled = false;
            resetButton.GetComponentInChildren<Text>().text = "Disable enemies";
        }

    }

    public void DisarmAll(){
        if (AllEnemies != null){
			for (int i = 0; i < AllEnemies.Length; i++){
				AllEnemies[i].Disarm();
			}
		}
    }
}
