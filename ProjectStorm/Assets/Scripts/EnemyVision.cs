﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour {

    public bool chasePlayer = false;
    
    private Transform lastKnown;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            chasePlayer = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            chasePlayer = false;
            lastKnown = other.transform;
        }
    }

    public Transform getLastKnown{
        get { return lastKnown; }
    }
}
