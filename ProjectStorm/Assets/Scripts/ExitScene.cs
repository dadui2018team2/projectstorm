﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitScene : MonoBehaviour {

    public string nextScene;

    private string startpointKey = "Checkpoint";

    /*private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.name == "Player") {
            UIManager.instance.LoadScene(nextScene);
        }
    }*/

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Player") {
            PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "_complete", 1);
            GameManager.EndLevel();
            UIManager.instance.LoadScene(nextScene);
            PlayerPrefs.SetInt(startpointKey, 0);
        }
    }
}
