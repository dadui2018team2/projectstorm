﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FOVEnabler : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Enemy") {
            other.GetComponent<FieldOfView>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == "Enemy") {
            other.GetComponent<FieldOfView>().enabled = false;
        }
    }
}
