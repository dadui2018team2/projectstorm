﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireFlicker : MonoBehaviour {

    public float maxFlickerTime = 0.2f;
    public Light[] flickerLights = new Light[0];

    private int flickerLightIndex = 0;
    private Coroutine flicker = null;

    private void Start() {
        foreach (Light light in flickerLights)
            light.enabled = false;

        flickerLights[flickerLightIndex].enabled = true;
    }

    void Update () {
        if (flicker == null) {
            float flickerTime = Random.Range(0f, maxFlickerTime);
            flicker = StartCoroutine(Flicker(flickerTime));
        }
	}

    private IEnumerator Flicker(float waitingTime) {
        int newFlickerLightIndex = flickerLightIndex;

        while (newFlickerLightIndex == flickerLightIndex) {
            newFlickerLightIndex = Random.Range(0, flickerLights.Length);
        }

        flickerLights[flickerLightIndex].enabled = false;
        flickerLights[newFlickerLightIndex].enabled = true;
        flickerLightIndex = newFlickerLightIndex;

        yield return new WaitForSeconds(waitingTime);

        flicker = null;
    }
}
