﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMovement : MonoBehaviour {

    public Transform lightTransform;
    public float displacementFactor;

    private Vector3 startingPosition;
    private bool hasMoved = false;

    private void Awake() {
        startingPosition = lightTransform.position;
    }

    void FixedUpdate () {
        if (!hasMoved) {
            Vector3 newPosition = lightTransform.position;
            newPosition += new Vector3(Random.Range(0, displacementFactor), Random.Range(0, displacementFactor), Random.Range(0, displacementFactor));
            lightTransform.position = newPosition;
            hasMoved = true;
        }
        else {
            lightTransform.position = startingPosition;
            hasMoved = false;
        }
	}
}
