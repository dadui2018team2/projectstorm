﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	private static GameManager instance;

    private static AudioBank audioBank;
    private MotionMatcher motionMatcher;
	private EnemyReset enemyReset;

	// Player
	private static Player player;
	private static bool playerOnShrooms = false;

	// Checkpoints
	private static CheckpointManager checkpointManager;

	// Collectable
	private static int collectExists = 0;
	private static int collectCollected = 0;
    private static RuneManager runemanager;

    // Score variables
    private static int deathCounter = 0;
    private float completionTime;
    private static string recentDeathsKey = "recentDeaths";
    private static string recentTimeKey = "recentTime";
    private static string topDeathsKey = "topDeaths";
    private static string topTimeKey = "topTime";

	private void Awake() {
		if (instance != null) Destroy(this);
		else instance = this;

		player = FindObjectOfType<Player>();
		checkpointManager = FindObjectOfType<CheckpointManager>();
        motionMatcher = FindObjectOfType<MotionMatcher>();
		enemyReset = gameObject.AddComponent<EnemyReset>();
        runemanager = FindObjectOfType<RuneManager>();
        audioBank = FindObjectOfType<AudioBank>();
    }

	private void Start() {
		enemyReset.FindAll();
		deathCounter = 0;
	}

    #region Runestone
    public static RuneManager GetRuneManager
    {
        get { return runemanager; }
    }
    #endregion

    #region Checkpoint
    public static CheckpointManager GetCheckpointManager{
		get { return checkpointManager; }
	}
	#endregion

    #region Player
    public static void PlayerDeath(){
        // Player death here
		
		player.canMove = false;
        deathCounter++;
        runemanager.DisableRuneUI();
        runemanager.RespawnRunes();
        // Find last checkpoint and move player there

        player.EndPowerUp();

        // Move player
        instance.StartCoroutine("AnimateDeath");
	}

    IEnumerator AnimateDeath() {
        motionMatcher.isDying = true;
        Animator anim = player.GetComponentInChildren<Animator>();

        //AkSoundEngine.PostEvent("Play_FriggDead", GameObject.FindGameObjectWithTag("Player"));
        player.GetComponent<AudioController>().PlayDeathSound();
        anim.CrossFade("death_front", 0.2f);
        yield return new WaitForSeconds(1.5f);
		PlayerToCheckpoint();
		enemyReset.ResetAllEnemies();
        player.canMove = true;
        anim.Play("idle");
        motionMatcher.isDying = false;
    }

	public static void PlayerToCheckpoint(){
		player.transform.position = checkpointManager.GetCurrentCheckpoint.GetSpawnPoint;
		player.GetCameraFollow.SnapCameraToPlayer();
	}

	// another version for CheckpointManager to call
	public static void PlayerToCheckpoint(Vector3 spawnPos){
		if (player != null){
			player.transform.position = spawnPos;
			player.GetCameraFollow.SnapCameraToPlayer();
		}
		else {
			player = FindObjectOfType<Player>();
			player.transform.position = spawnPos;
			player.GetCameraFollow.SnapCameraToPlayer();
		}
	}

	public static Player GetPlayer{
		get {return player;}
	}

	public static bool IsPlayerOnShrooms{
		get {return player.isOnShroom;}
	}

	#endregion

	#region Collectables
	public static void CollectExists(){
		// This function is basically for collectables to say: "I exist!! :D".
		collectExists++;
	}

	public static void AddCollect(){
		collectCollected++;
	}

	public static int GetCollectExists{
		get{return collectExists;}
	}

	public static int GetCollectCollected{
		get{return collectCollected;}
	}

    #endregion

    public static AudioBank GetAudioBank
    {
        get { return audioBank; }
    }

    public static void EndLevel()
    {
        PlayerPrefs.SetInt(recentDeathsKey, deathCounter);
        PlayerPrefs.SetFloat(recentTimeKey, Time.timeSinceLevelLoad);
		runemanager.UpdateRuneList();
    }
}
