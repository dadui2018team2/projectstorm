﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killzone : MonoBehaviour {

	[Tooltip ("Instead of killing the player, reverse will kill guards. Use for player berserk.")]
	public bool reverse;

    private Animator anim;
    private MotionMatcher motionMatcher;

    private void Awake() {
        if (!reverse)
            return;

        anim = gameObject.transform.parent.GetComponentInChildren<Animator>();
        motionMatcher = FindObjectOfType<MotionMatcher>();
    }

    private void OnCollisionEnter(Collision other) {
		if (reverse){
            if (other.gameObject.GetComponent<EnemyBehaviour>()) { // kill (disable) enemy
                StartCoroutine("PlayerAttack");
                other.gameObject.SetActive(false);
            } 
		}
		else {
			if (other.gameObject.tag.Equals("Player")) GameManager.PlayerDeath();
		}
		
	}

	private void OnTriggerEnter(Collider other) {
		if (reverse){
            if (other.gameObject.GetComponent<EnemyBehaviour>()) { // kill (disable) enemy
                StartCoroutine("PlayerAttack");
                other.gameObject.GetComponent<EnemyBehaviour>().EnemyDeath();
            } 
		}
		else if (other.tag.Equals("Player")){
			GameManager.PlayerDeath();
		}
	}

    IEnumerator PlayerAttack() {
        motionMatcher.isAttacking = true;

        anim.CrossFade("attack", 0.2f);

        yield return new WaitForSeconds(0.5f);

        motionMatcher.isAttacking = false;
        motionMatcher.ForceCurrentAnimation();
    }
}
