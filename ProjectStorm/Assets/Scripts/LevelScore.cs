﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelScore : MonoBehaviour {
    public Text Deaths;
    public Text TimeTaken;
    public float TotalScore;
    private string recentDeathsKey = "recentDeaths";
    private string recentTimeKey = "recentTime";
    public Sprite oneStar;
    public Sprite twoStar;
    public Sprite threeStar;
    public Image Stars;
    public int highReq = 170;
    public int bhighReq = 169;
    public int midReq = 90;
    public int lowReq = 89;


    void Start () {
        Deaths.text += " " +PlayerPrefs.GetInt(recentDeathsKey, 0).ToString();
        TimeTaken.text += " " +PlayerPrefs.GetFloat(recentTimeKey, 0).ToString();
        TotalScore = (PlayerPrefs.GetFloat(recentTimeKey, 0)+1) * (PlayerPrefs.GetInt(recentDeathsKey, 1)+1);
        Debug.Log(PlayerPrefs.GetFloat(recentTimeKey));

        if( TotalScore >= highReq ){
            Stars.sprite = oneStar;
        }
        if (TotalScore <= bhighReq && TotalScore >= midReq)
        {
            Stars.sprite = twoStar;
        }
        if (TotalScore <= lowReq)
        {
            Stars.sprite = threeStar;
        }
    }
}
