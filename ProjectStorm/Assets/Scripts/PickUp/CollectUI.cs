﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectUI : MonoBehaviour {

	public Text collectText;
	
	// Update is called once per frame
	void Update () {
		if (collectText != null){
			int current = GameManager.GetCollectCollected;
			int total = GameManager.GetCollectExists;
			collectText.text = current + "/" + total;
		}
		
	}
}
