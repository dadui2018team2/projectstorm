﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

	private ItemPickUp itemPickUp;

    private AudioSource audioSource;
    private AudioClip myClip;

	// Use this for initialization
	void Start () {
		itemPickUp = transform.parent.GetComponent<ItemPickUp>();
	}

	private void OnTriggerEnter(Collider other) {
        if (audioSource == null)
            audioSource = transform.parent.GetComponent<AudioSource>();

		if (other.tag.Equals("Player")){
			itemPickUp.PickedUp();
		}
	}
}
