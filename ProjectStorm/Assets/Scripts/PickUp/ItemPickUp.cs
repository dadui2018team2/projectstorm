﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : MonoBehaviour {

	[Tooltip ("Is the object a power up? If not, its a collectable.")]
	public bool isPowerUp;
	[Tooltip ("If the object is a power up, how long for it to respawn? 0 = no respawn.")]
	public float respawnTime;
	[Tooltip ("If the object is a power up, how long is it active on player?")]
	public float powerTime;
	public Vector3 itemRotation;
	private Item item;
	private float timer = 0.0f;
	private bool respawning = false;
	private RunestoneSound runestoneSound;
	private AudioSource audioSource;
    private AudioClip myClip;

    public int runeIndex;

	// Use this for initialization
	void Awake () {
		item = GetComponentInChildren<Item>();
		if (!isPowerUp){
			// Is a collectable
			GameManager.CollectExists();
		}
		runestoneSound = GetComponent<RunestoneSound>();
		audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (respawning){
			if (timer < Time.time) Reactivate();
		}
		if (item.gameObject.activeSelf){
			item.transform.Rotate(itemRotation, Space.World);
		}
	}

	public void PickedUp(){
		if (isPowerUp){
			GameManager.GetPlayer.PowerUp(powerTime);
			if (respawnTime != 0){
				timer = respawnTime + Time.time;
				respawning = true;
				item.gameObject.SetActive(false);
			}
			if (runestoneSound != null) runestoneSound.enabled = false;
			if (myClip == null)
                    myClip = GameManager.GetAudioBank.player_beserk;

            audioSource.clip = myClip;
            audioSource.Play();
		}
		else{
            GameManager.GetRuneManager.ActivateRune(runeIndex);
			//GameManager.AddCollect();
			item.gameObject.SetActive(false);
			if (runestoneSound != null) runestoneSound.enabled = false;
			if (myClip == null)
                    myClip = GameManager.GetAudioBank.runestone_pickUp;

            audioSource.clip = myClip;
            audioSource.Play();
		}
	}

	public void Reactivate(){
		if (!isPowerUp){
			if (runestoneSound != null) runestoneSound.enabled = true;
		}

		item.gameObject.SetActive(true);
		respawning = false;
	}
}
