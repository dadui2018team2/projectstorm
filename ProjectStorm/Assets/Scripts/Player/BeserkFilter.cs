﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeserkFilter : MonoBehaviour {

    public float lerpChange = 0.04f;
    
    private float lerpValue = 0.0f;

    private Color colorNormal;
    private Color colorBeserk;

	void Start () {
        colorNormal = RenderSettings.ambientSkyColor;
        colorBeserk = Color.red;
	}

	void Update () {
        if (GameManager.IsPlayerOnShrooms && lerpValue < 1f) {
            lerpValue += lerpChange;
            RenderSettings.ambientSkyColor = Color.Lerp(colorNormal, colorBeserk, lerpValue);
        }

        if (!GameManager.IsPlayerOnShrooms && lerpValue > 0f) {
            lerpValue -= lerpChange;
            RenderSettings.ambientSkyColor = Color.Lerp(colorNormal, colorBeserk, lerpValue);
        }

    }
}
