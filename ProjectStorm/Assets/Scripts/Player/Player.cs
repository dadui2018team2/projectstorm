﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public bool canMove = true;

	// Material
	public Material defaultMaterial;
	public Material powerMaterial;
	private MeshRenderer meshRenderer;

	private PlayerMove playerMove;
	private CameraFollow cameraFollow;

	// Berserk
	public GameObject killCone;
	private bool isPoweredUp = false;
	private float powerTimer = 0.0f;

	private void Awake() {
		meshRenderer = GetComponent<MeshRenderer>();
		playerMove = GetComponent<PlayerMove>();
		cameraFollow = GetComponent<CameraFollow>();
		killCone.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if(canMove)
		    playerMove.MovePlayer();

		if (isPoweredUp){
			if (powerTimer < Time.time){
				// End PowerUp
				EndPowerUp();
			}
		}
	}

	public void PowerUp(float powerTime){
		isPoweredUp = true;
		powerTimer = Time.time + powerTime;
		meshRenderer.material = powerMaterial;
		killCone.SetActive(true);
	}

	public void EndPowerUp(){
		isPoweredUp = false;
		meshRenderer.material = defaultMaterial;
		killCone.SetActive(false);
	}

	public bool isOnShroom{
		get { return isPoweredUp; }
	}

	public CameraFollow GetCameraFollow{
		get { return cameraFollow; }
	}

	public float CurrentSpeed{
		get { return playerMove.CurrentSpeed; }
	} 

	public bool Sprinting{
		get { return playerMove.isSprinting; }
	}
}
