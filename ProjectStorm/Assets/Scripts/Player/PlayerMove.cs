﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Add this script to player object. Give camera "MainCamera" tag.
 */

[RequireComponent (typeof(CharacterController))]

public class PlayerMove : MonoBehaviour {

	public float speed;
	public float sprintSpeed;
	public float rotationSpeed;
	public float gravity;

	public VirtualJoystick virtualJoystick;

	private CharacterController cc;
	private bool sprint = false;

    private AudioController ac;
    public string playerFootsteps;

	private void Awake() {
		cc = GetComponent<CharacterController>();
        ac = gameObject.GetComponent<AudioController>();
	}

	public void MovePlayer() {
		// Get input
		Vector3 targetMove = Vector3.zero;
		if (virtualJoystick != null){
			targetMove = virtualJoystick.JoystickInput();
			sprint = virtualJoystick.isSprintDown;
		}
		if (targetMove == Vector3.zero){
			targetMove = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            if (Input.GetKeyDown(KeyCode.LeftShift)) {
                sprint = true;
            }
            else if(Input.GetKeyUp(KeyCode.LeftShift))
                sprint = false;
        }

        if (cc.velocity.magnitude > 0) {
            ac.PlayFootsteps(playerFootsteps);
            ac.SetSprint(sprint);
        }

        // Orientate to camera view
        targetMove = Transform2CameraView(targetMove);

		// Set max magnitude to 1.0f
		if (targetMove.magnitude > 1.0f) targetMove= targetMove.normalized;
		
		// Apply speed & gravity
		Vector3 move = targetMove;
		if (sprint) move *= sprintSpeed;
		else move *= speed;
		move.y += gravity * Time.deltaTime + Mathf.Min(cc.velocity.y, 0);

		cc.Move(move * Time.deltaTime);

		// Rotation of character
        if (move != Vector3.zero){
            Vector3 lookDir = move;
            lookDir.y = 0;
            Vector3 newLook = Vector3.RotateTowards(transform.forward, lookDir, Mathf.Rad2Deg * rotationSpeed * Time.deltaTime, 0);
            transform.rotation = Quaternion.LookRotation(newLook);
        }
	}

	private Vector3 Transform2CameraView(Vector3 dir){
		Vector3 newDir = Camera.main.transform.TransformDirection(dir);
		newDir.y = 0;
		newDir = newDir.normalized * dir.magnitude;

		return newDir;
	}

	public bool isSprinting{
		set {sprint = value;}
		get { return sprint; }
	}

	public float CurrentSpeed{
		get { return cc.velocity.magnitude; }
	}
}
