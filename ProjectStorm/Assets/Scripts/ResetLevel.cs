﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResetLevel : MonoBehaviour {

    public Text playerSpeed;
    public Text checkpointIndex;
    public Button DisableEnemiesButton;

    CharacterController playerController;

    private int currentCheckpoint;
    private EnemyReset enemyReset;

    private void Awake() {
        enemyReset = gameObject.AddComponent<EnemyReset>();
    }

    // Use this for initialization
    void Start () {
        enemyReset.FindAll();
        playerController = GameManager.GetPlayer.gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        //playerSpeed.text = "Player speed: " + Mathf.Round(playerController.velocity.magnitude);
        //currentCheckpoint = GameManager.GetCheckpointManager.GetCurrentCheckpoint.GetIndex;
        //Debug.Log(managerScript.GetCurrentCheckpoint);
        //checkpointIndex.text = "Checkpoint: " + currentCheckpoint;
		
	}

    public void DisableEnemyButton() {
        enemyReset.DisableEnemies(DisableEnemiesButton);
    }

   public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public int SetIndex
    {
        set { currentCheckpoint = value; }
    }
}
