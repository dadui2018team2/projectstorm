﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RuneManager : MonoBehaviour {

    public GameObject[] runeObjects;

    public GameObject runeUI1;
    public GameObject runeUI2;
    public GameObject runeUI3;

    public int[] runesPickedUp;
    
	void Start () {
        SetRunesActive();

        runesPickedUp = new int[runeObjects.Length];
        DisableRuneUI();
        //RespawnRunes();
        //Debug.Log(SceneManager.GetActiveScene().name + "rune" + 0);
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R)){
            PlayerPrefs.DeleteAll();
            //Debug.Log("Deleted");
        } 
        if (Input.GetKeyDown(KeyCode.E)){
            SetRunesActive();
        }
    }

    public void ActivateRune(int index) {
        int currentCheckpoint = GameManager.GetCheckpointManager.GetCurrentCheckpoint.GetIndex;
        int runeIndex = currentCheckpoint * 3 + index - 1;
        if (runeIndex < runeObjects.Length){
            runesPickedUp[runeIndex] = 1; // Rune index starts at 1, not 0
        }
        switch (index) {
            case 1:
                runeUI1.SetActive(true);
                break;
            case 2:
                runeUI2.SetActive(true);
                break;
            case 3:
                runeUI3.SetActive(true);
                break;
            default:
                Debug.Log("This rune does not exist");
                break;
        }
    }

    public void DisableRuneUI() {
        runeUI1.SetActive(false);
        runeUI2.SetActive(false);
        runeUI3.SetActive(false);
    }

    public void RespawnRunes() {
        int currentCheckpoint = GameManager.GetCheckpointManager.GetCurrentCheckpoint.GetIndex;
        
        for(int i = currentCheckpoint * 3; i < (currentCheckpoint * 3) + 3; i++) {
            if (i < runeObjects.Length){
            runeObjects[i].SetActive(true);
            runeObjects[i].GetComponent<ItemPickUp>().Reactivate();
            runesPickedUp[i] = 0;
            } 
        }
    }

    public void UpdateRuneList() {
        //Debug.Log("Updating runes");
        for (int i = 0; i < 3; i++) {
            int runeIndex = GameManager.GetCheckpointManager.GetCurrentCheckpoint.GetIndex * 3 + i;
            if (PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "rune" + runeIndex, 0) != 1){
                PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "rune" + runeIndex, runesPickedUp[runeIndex]);
                //Debug.Log("Key: " + SceneManager.GetActiveScene().name + "rune" + runeIndex + "Value: " + runesPickedUp[runeIndex]);
            }
        }
        PlayerPrefs.SetString("LastScene", SceneManager.GetActiveScene().name);
    }

    public void SetRunesActive() {
        for (int i = 0; i < runeObjects.Length; i++) {
            /*int isItActive = PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "rune" + i, 0);
            Debug.Log("check rune " + i + ". Status = " + isItActive);
            if (isItActive == 0) {
                runeObjects[i].SetActive(true);
                Debug.Log("Active" + i);
            }
            else {
                runeObjects[i].SetActive(false);
                Debug.Log("Disable" + i);
            } */
            runeObjects[i].SetActive(true);
            runeObjects[i].GetComponent<ItemPickUp>().Reactivate();
        }
    }

    public void PrintRuneList() {
        foreach (int runeBool in runesPickedUp) {
            //Debug.Log(runeBool);
        }
    }

    public void DisablePreviousRunes() {
        int currentCheckpoint = GameManager.GetCheckpointManager.GetCurrentCheckpoint.GetIndex;
        if (currentCheckpoint > 0){
            for (int i = (currentCheckpoint) * 3; i < ((currentCheckpoint) * 3) + 3; i++) {
                runeObjects[i].SetActive(false);
            }
        }
        
    }

    public void DeletePlayerprefs() {
        PlayerPrefs.DeleteAll();
    }
}
