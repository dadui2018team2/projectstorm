﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunestoneSound : MonoBehaviour {

    public float initialCounter;
    public float currentCounter;

    private AudioSource audioSource;

    private AudioClip sound_emitting;

    public void Update() {
        PlaySound();
    }

    private void PlaySound() {
        if (audioSource == null)
            audioSource = GetComponent<AudioSource>();

        if (sound_emitting == null)
            sound_emitting = GameManager.GetAudioBank.runestone_emitting;

        currentCounter -= Time.deltaTime;

        if (currentCounter <= 0) {
            //AkSoundEngine.PostEvent("Play_RunestoneHaer", gameObject);
            audioSource.clip = sound_emitting;
            audioSource.Play();

            currentCounter = initialCounter;
        }
    }
}
