﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShroomFilter : MonoBehaviour {

	public bool show = false;
	public float speed;

	private SpriteRenderer spriteRenderer;
	private Animator animator;

	private void Awake() {
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameManager.IsPlayerOnShrooms && !show) {
            spriteRenderer.enabled = false;
            return;
        }
		spriteRenderer.enabled = true;
		animator.speed = speed;
	}
}
