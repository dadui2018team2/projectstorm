﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShroomLight : MonoBehaviour {

    private Color originalAmbientColor;

    public Color col1 = Color.magenta;
    public Color col2 = Color.blue;

    public float lerpValue = 0.1f;
    private float lerp = 0f;

    private bool lerpPositive = true;

    private void Awake() {
        originalAmbientColor = RenderSettings.ambientSkyColor;
    }

    private void Update() {
        if (!GameManager.IsPlayerOnShrooms) {
            if (RenderSettings.ambientSkyColor != originalAmbientColor)
                RenderSettings.ambientSkyColor = originalAmbientColor;
            return;
        }

        if (lerpPositive) {
            lerp += lerpValue;
            RenderSettings.ambientSkyColor = Color.Lerp(col1, col2, lerp);
            if (lerp >= 1f)
                lerpPositive = false;
        }
        else if (!lerpPositive) {
            lerp -= lerpValue;
            RenderSettings.ambientSkyColor = Color.Lerp(col1, col2, lerp);
            if (lerp <= 0f)
                lerpPositive = true;
        }
    }
}
