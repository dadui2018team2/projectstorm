﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    // Scene names
    [Header ("Level Names")]
    public string level1Name;
    public string level2Name;
    public string level3Name;

    public static UIManager instance;

    [Header("UI elements")]
    public GameObject level1;
    public GameObject level2;
    public Button level2_button;

    public Button checkpoint_1;
    public Button checkpoint_2;
    public Button checkpoint_3;

    public Button checkpoint_21;
    public Button checkpoint_22;
    public Button checkpoint_23;

    [Header("UI level numbers")]
    public GameObject lvl1Number;
    public GameObject lvl2Number;
    public GameObject cp2Number;
    public GameObject cp3Number;
    public GameObject cp22Number;
    public GameObject cp23Number;

    [Header("Locks")]
    public GameObject lvl2lock;
    public GameObject cp2lock;
    public GameObject cp3lock;
    public GameObject cp21lock;
    public GameObject cp22lock;
    public GameObject cp23lock;

    [Header("Runestones")]
    public GameObject[] runeStones;
    


    [Header("Sound and music elements")]
    public Slider musicSlider;
    public Slider soundSlider;

    private string startpointKey = "Checkpoint";

    private void Start() {
        if (instance == null) {
            instance = this;
        }
        else {
            Destroy(this);
        }

        if (SceneManager.GetActiveScene().name == "LevelSelect") {
            if (PlayerPrefs.GetInt("Scene1_complete", 0) == 1)
            {
                level2_button.interactable = true;
                lvl2lock.SetActive(false);
                lvl2Number.SetActive(true);
                
            }
            else if (PlayerPrefs.HasKey("Scene2_checkpoint") == false) {
                level2_button.interactable = false;
                lvl2lock.SetActive(true);
                lvl2Number.SetActive(false);
            }
            return;
        }
        else {
            for (int i = 0; i < runeStones.Length; i++) {
                string sceneName = PlayerPrefs.GetString("LastScene");
                int shouldActive = PlayerPrefs.GetInt(sceneName + "rune" + i);

                if (shouldActive == 1) runeStones[i].gameObject.SetActive(true);
                else runeStones[i].gameObject.SetActive(false);
            }
        }
    }

    public void OpenLevelPanel(int num) {
        switch (num) {
            case 1:
                Debug.Log("opening level 1");
                level1.SetActive(true);
                DisableCheckpointButtons(1, PlayerPrefs.GetInt("Scene1_checkpoint", 0));
                break;
            case 2:
                Debug.Log("opening level 2");
                level2.SetActive(true);
                DisableCheckpointButtons(2, PlayerPrefs.GetInt("Scene2_checkpoint", 0));
                break;
            default:
                Debug.Log("this level doesn't exist");
                break;
        }
    }

    private void DisableCheckpointButtons(int level, int latestCheckpoint) {
        if (SceneManager.GetActiveScene().name != "LevelSelect")
            return;

        switch (level) {
            case 1:
                if (latestCheckpoint == 0) {
                    checkpoint_2.interactable = false;
                    checkpoint_3.interactable = false;
                    cp2lock.SetActive(true);
                    cp3lock.SetActive(true);
                    cp2Number.SetActive(false);
                    cp3Number.SetActive(false);
                }
                else if (latestCheckpoint == 1) {
                    checkpoint_3.interactable = false;
                    cp2lock.SetActive(false);
                    cp2Number.SetActive(true);
                    cp3lock.SetActive(true);
                    cp3Number.SetActive(false);
                }
                else if (latestCheckpoint == 2) {
                    cp2lock.SetActive(false);
                    cp2Number.SetActive(true);
                    cp3lock.SetActive(false);
                    cp3Number.SetActive(true);
                }
                break;

            case 2:
                if (latestCheckpoint == 0) {
                    checkpoint_22.interactable = false;
                    checkpoint_23.interactable = false;             
                    cp22lock.SetActive(true);
                    cp23lock.SetActive(true);
                    cp22Number.SetActive(false);
                    cp23Number.SetActive(false);


                }
                else if (latestCheckpoint == 1) {
                    checkpoint_23.interactable = false;
                    cp22lock.SetActive(false);
                    cp22Number.SetActive(true);
                    cp23lock.SetActive(true);
                    cp23Number.SetActive(false);
                }
                else if (latestCheckpoint == 2) {
                    cp22lock.SetActive(false);
                    cp22Number.SetActive(true);
                    cp23lock.SetActive(false);
                    cp23Number.SetActive(true);
                }
                break;

            default:
                break;
        }
    }

    public void CloseLevelPanel(GameObject go) {
        Transform parent = go.transform.parent;
        parent.gameObject.SetActive(false);
    }

    public void LoadScene(string scene) {
        SceneManager.LoadScene(scene);
        if (SceneManager.GetActiveScene().name.Contains("Scene1") || SceneManager.GetActiveScene().name.Contains("Scene2")) {
            //AkSoundEngine.PostEvent("Stop_Ambience", gameObject);
            //AkSoundEngine.PostEvent("Stop_Music", gameObject);
        }
    }

    public void LoadSceneLevel1(int checkpoint){
        PlayerPrefs.SetInt(startpointKey, checkpoint);
        LoadScene(level1Name);
    }

    public void LoadSceneLevel2(int checkpoint){
        PlayerPrefs.SetInt(startpointKey, checkpoint);
        LoadScene(level2Name);
    }

    public void LoadSceneLevel3(int checkpoint){
        PlayerPrefs.SetInt(startpointKey, checkpoint);
        LoadScene(level2Name);
    }

    public void QuitApplication() {
        Application.Quit();
    }

    public void PlayButtonClick() {
        //AkSoundEngine.PostEvent("Play_ClickingSound", gameObject);
    }

    public void SetMusicVolume() {
        AkSoundEngine.SetRTPCValue("Music_Volume", (musicSlider.value*100));
        PlayerPrefs.SetFloat("Music_Volume", musicSlider.value);
    }

    public void SetSoundVolume() {
        AkSoundEngine.SetRTPCValue("Sound_Volume", (soundSlider.value*100));
        PlayerPrefs.SetFloat("Sound_Volume", soundSlider.value);
    }

    public void PlaceRunes(string buttonName) {
        if(buttonName == "Level1") {
            for (int i = 0; i < runeStones.Length; i++) {
                int shouldActive = PlayerPrefs.GetInt("Scene1rune"+i);

                if (shouldActive == 1) runeStones[i].gameObject.SetActive(true);
                else runeStones[i].gameObject.SetActive(false);
            }
        }
        else if(buttonName == "Level2") {
            for (int i = 0; i < runeStones.Length; i++) {
                int shouldActive = PlayerPrefs.GetInt("Scene2rune" + i);

                if (shouldActive == 1) runeStones[i].gameObject.SetActive(true);
                else runeStones[i].gameObject.SetActive(false);
            }
        }
    }
}
