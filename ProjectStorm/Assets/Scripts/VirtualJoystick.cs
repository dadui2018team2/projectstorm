﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour{

	public bool mouseMove = false; // is for testing the joystick with the mouse only, can be removed later.

	[Tooltip ("How big is the dead zone? 1 = screen height.")]
	public float deadZone;
	public Button joystickButton;
	public Image joystickImage;
	public Image joystickZeroImage;
	public float imageMoveDistance;
	public Button sprintButton;

	private int moveFinger;
	private bool moveFingerDown = false;
	private bool sprint = false;

	private Vector2 joystickPosDefault;
	private Vector2 joystickPos;

	private void Start() {
		joystickPos = joystickImage.transform.position;
		joystickPosDefault = joystickPos;
		MoveJoystickImage(Vector2.zero);
	}

	private void MoveJoystickImage(Vector2 pos){
		Vector2 lerpedPos = Vector2.Lerp(Vector2.zero, (pos.normalized * imageMoveDistance) * Screen.height, pos.magnitude);
		Vector2 targetPos = joystickPos + lerpedPos;
		joystickImage.transform.position = targetPos;
	}

	public void OnMoveDown(){
		foreach (Touch touch in Input.touches){
			if (touch.phase == TouchPhase.Began){
				if (!moveFingerDown){
					moveFinger = touch.fingerId;
					moveFingerDown = true;
					joystickPos = touch.position;
					joystickZeroImage.transform.position = joystickPos;
				}
			}
		}
		if (mouseMove){
			moveFingerDown = true;
		}
	}

	public void OnMoveUp(){
		moveFingerDown = false;
		joystickPos = joystickPosDefault;
		joystickZeroImage.transform.position = joystickPos;
		MoveJoystickImage(joystickPos);
	}

	public Vector3 JoystickInput(){
		// PC testing
		if (moveFingerDown && mouseMove){
			Vector2 touchPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - joystickPos;
			Vector3 input = new Vector3(touchPos.x, 0, touchPos.y);
			if (input.magnitude < deadZone * Screen.height) input = Vector3.zero;
			input = input.normalized;

			MoveJoystickImage(new Vector2(input.x, input.z));
			return input;
		}

		// Android
		if (moveFingerDown){
			Touch touch = Input.GetTouch(moveFinger);
			if (touch.phase == TouchPhase.Ended) {
				moveFingerDown = false;
				MoveJoystickImage(Vector2.zero);
				return Vector3.zero;
			}
			Vector2 touchPos = touch.position - joystickPos;
			Vector3 input = new Vector3(touchPos.x, 0, touchPos.y);
			if (touchPos.magnitude < deadZone * Screen.height) input = Vector3.zero;
			MoveJoystickImage(new Vector2(input.x, input.z));
			return input.normalized;
		}
		MoveJoystickImage(Vector2.zero);
		return Vector3.zero;
	}

	public void OnSprintDown(){
		sprint = true;
	}

	public void OnSprintUp(){
		sprint = false;
	}

	public bool isSprintDown{
		get {return sprint;}
	}
}
