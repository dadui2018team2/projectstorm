/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_ALERTSOUNDS_HEARD = 3966544705U;
        static const AkUniqueID PLAY_ALERTSOUNDS_SEEN = 2755638158U;
        static const AkUniqueID PLAY_AMBIENCE = 278617630U;
        static const AkUniqueID PLAY_CHECKPOINT1 = 1493052601U;
        static const AkUniqueID PLAY_CLICKINGSOUND = 3757023739U;
        static const AkUniqueID PLAY_ENEMYDEATHSOUND = 1075033933U;
        static const AkUniqueID PLAY_ENEMYFOOTSTEPSWALK = 1846669540U;
        static const AkUniqueID PLAY_FIRE = 3015324718U;
        static const AkUniqueID PLAY_FRIGGDEAD = 4269332719U;
        static const AkUniqueID PLAY_FRIGGFOOTSTEPSWALK = 3381991781U;
        static const AkUniqueID PLAY_MUSICTEST = 1414448543U;
        static const AkUniqueID PLAY_RUNESTONEHAER = 2828976429U;
        static const AkUniqueID PLAY_RUNESTONES = 951567808U;
        static const AkUniqueID PLAY_SHROOMS = 3772451993U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMBIENCEZONES
        {
            static const AkUniqueID GROUP = 715804450U;

            namespace STATE
            {
                static const AkUniqueID BEACH = 4075332698U;
                static const AkUniqueID CITY = 3888786832U;
                static const AkUniqueID FOREST = 491961918U;
            } // namespace STATE
        } // namespace AMBIENCEZONES

        namespace LIFESTATE
        {
            static const AkUniqueID GROUP = 761044930U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEAD = 2044049779U;
                static const AkUniqueID SHROOMS = 3983930604U;
                static const AkUniqueID UI = 1551306167U;
            } // namespace STATE
        } // namespace LIFESTATE

        namespace MUSIC_STAGES
        {
            static const AkUniqueID GROUP = 1424345200U;

            namespace STATE
            {
                static const AkUniqueID HIGH = 3550808449U;
                static const AkUniqueID LOW = 545371365U;
            } // namespace STATE
        } // namespace MUSIC_STAGES

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MUSIC_VOLUME = 1006694123U;
        static const AkUniqueID SOUND_VOLUME = 495870151U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID SOUNDBANK_STORM = 729147818U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SOUND = 623086306U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
