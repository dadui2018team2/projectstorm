State Group	ID	Name			Wwise Object Path	Notes
	715804450	AmbienceZones			\Default Work Unit\AmbienceZones	
	761044930	LifeState			\Default Work Unit\LifeState	
	1424345200	Music_Stages			\Default Work Unit\Music_Stages	

State	ID	Name	State Group			Notes
	0	None	AmbienceZones			
	491961918	Forest	AmbienceZones			
	3888786832	City	AmbienceZones			
	4075332698	Beach	AmbienceZones			
	0	None	LifeState			
	655265632	Alive	LifeState			
	2044049779	Dead	LifeState			
	3983930604	Shrooms	LifeState			
	0	None	Music_Stages			
	545371365	Low	Music_Stages			
	1182670505	Mid	Music_Stages			
	3550808449	High	Music_Stages			

Game Parameter	ID	Name			Wwise Object Path	Notes
	495870151	Sound_Volume			\Default Work Unit\Sound_Volume	
	1006694123	Music_Volume			\Default Work Unit\Music_Volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	623086306	Sound			\Default Work Unit\Master Audio Bus\Sound	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3991942870	Music			\Default Work Unit\Master Audio Bus\Music	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

