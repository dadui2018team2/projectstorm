Event	ID	Name			Wwise Object Path	Notes
	278617630	Play_Ambience			\Default Work Unit\Play_Ambience	
	951567808	Play_RuneStones			\Default Work Unit\Play_RuneStones	
	1075033933	Play_EnemyDeathSound			\Default Work Unit\Play_EnemyDeathSound	
	1414448543	Play_Musictest			\Default Work Unit\Play_Musictest	
	1493052601	Play_CheckPoint1			\Default Work Unit\Play_CheckPoint1	
	1846669540	Play_EnemyFootstepsWalk			\Default Work Unit\Play_EnemyFootstepsWalk	
	2755638158	Play_AlertSounds_Seen			\Default Work Unit\Play_AlertSounds_Seen	
	2828976429	Play_RuneStoneHaer			\Default Work Unit\Play_RuneStoneHaer	
	3015324718	Play_Fire			\Default Work Unit\Play_Fire	
	3381991781	Play_FriggFootstepsWalk			\Default Work Unit\Play_FriggFootstepsWalk	
	3757023739	Play_ClickingSound			\Default Work Unit\Play_ClickingSound	
	3772451993	Play_Shrooms			\Default Work Unit\Play_Shrooms	
	3966544705	Play_AlertSounds_Heard			\Default Work Unit\Play_AlertSounds_Heard	
	4269332719	Play_FriggDead			\Default Work Unit\Play_FriggDead	

State Group	ID	Name			Wwise Object Path	Notes
	715804450	AmbienceZones			\Default Work Unit\AmbienceZones	
	761044930	LifeState			\Default Work Unit\LifeState	

State	ID	Name	State Group			Notes
	0	None	AmbienceZones			
	491961918	Forest	AmbienceZones			
	3888786832	City	AmbienceZones			
	4075332698	Beach	AmbienceZones			
	0	None	LifeState			
	655265632	Alive	LifeState			
	1551306167	UI	LifeState			
	2044049779	Dead	LifeState			
	3983930604	Shrooms	LifeState			

Custom State	ID	Name	State Group	Owner		Notes
	54222345	UI	LifeState	\Effects\Default Work Unit\Shrooms_Flanger		
	77129219	City	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience2		
	393516248	Forest	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1		
	596080972	Beach	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience3		
	601621119	Forest	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience3		
	718492441	City	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1		
	746982723	Dead	LifeState	\Effects\Default Work Unit\Shrooms_Flanger		
	823171311	Beach	AmbienceZones	\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience2		
	924652620	Alive	LifeState	\Effects\Factory Effects\Wwise RoomVerb\Cathedrals\Holy		
	933267726	Dead	LifeState	\Effects\Factory Effects\Wwise RoomVerb\Cathedrals\Holy		
	955181237	UI	LifeState	\Effects\Factory Effects\Wwise RoomVerb\Cathedrals\Holy		
	1032739062	Alive	LifeState	\Effects\Default Work Unit\Shrooms_Flanger		

Effect plug-ins	ID	Name	Type				Notes
	3164629247	Holy	Wwise RoomVerb			
	3276301890	Shrooms_Flanger	Wwise Flanger			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	22277457	UI_Click	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\UI_Click_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\ClickingSound\UI_Click		2379
	31779568	Shrooms3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Shrooms3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Shrooms\Shrooms3		23537
	35473081	EnemyHeard6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard6_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard6		11012
	42036802	RuneStoneCall4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStoneCall4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\RuneStoneHaer\RuneStoneCall4		30900
	45923341	FriggDeath5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath5		10252
	48610134	Forest_Soundscape_Loop_WindInTrees	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Forest_Soundscape_Loop_WindInTrees_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience2\HowlingInTrees\Forest_Soundscape_Loop_WindInTrees		1889796
	49785880	EnemyDeadYell1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell1		10726
	50339531	FriggFootsteps1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps1_14038C5E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps1		4866
	51809878	LynMellem	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\LynMellem_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience2\ThunderSounds\LynMellem		170197
	59122536	EnemySeen6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen6_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen6		7441
	73793228	EnemyFootsteps6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps6_80E121B5.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps6		2747
	86593727	LynT�t2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\LynT�t2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience3\ThunderSound\LynT�t2		106868
	90175549	EnemyHeard1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard1		4705
	93141312	EnemyClothes2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyClothes2_21226F41.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyClothes\EnemyClothes2		4957
	109263273	Shrooms4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Shrooms4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Shrooms\Shrooms4		23914
	115804210	MusicMiniGameIIHigh	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\MusicStates\MusicMiniGameIILow_36EEBDE7.wem		\Interactive Music Hierarchy\MusicTest\Musictest\MusicStates\MusicMiniGameIIHigh		432631
	129822462	Check Point1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Check Point1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\CheckPoint1\Check Point1		43771
	142113975	EnemyHeard5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard5		4726
	148550232	FriggDeath3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath3		10350
	149328977	FriggClothes5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes5_5B8D7662.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes5		6801
	192259622	FriggFootsteps5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps5_03F9B957.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps5		5118
	197510898	EnemyHeard7	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard7_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard7		10835
	215771704	FriggFootsteps3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps3_219977FE.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps3		5361
	233976973	LynLangtV�k	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\LynLangtV�k_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1\ThunderSounds\LynLangtV�k		124133
	249330307	FriggFootsteps4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps4_CA98F260.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps4		4249
	280726249	Shrooms2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Shrooms2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Shrooms\Shrooms2		22348
	289825580	EnemyClothes6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyClothes6_745C0A1F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyClothes\EnemyClothes6		5051
	298285629	EnemyClothes3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyClothes3_DED4257E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyClothes\EnemyClothes3		5501
	307483439	EnemySeen3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen3		5108
	310594448	EnemyFootsteps2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps2_562F9214.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps2		3260
	315084494	EnemyFootsteps5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps5_AC09F6D1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps5		2862
	345134092	FriggDeath1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath1		10372
	378855681	EnemyHeard8	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard8_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard8		11976
	388485447	FriggClothes3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes3_BA646D7E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes3		8946
	396296125	EnemyClothes5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyClothes5_E5585AB8.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyClothes\EnemyClothes5		5451
	409870213	FriggKill4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggKill4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\FriggYells\FriggKill4		8842
	435755184	RuneStoneCall3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStoneCall3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\RuneStoneHaer\RuneStoneCall3		35799
	447644436	FriggClothes1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes1_06160169.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes1		7006
	458406616	FriggClothes6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes6_BD9103CD.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes6		5505
	464636609	RuneStoneCall2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStoneCall2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\RuneStoneHaer\RuneStoneCall2		31753
	476299648	MusicMiniGameIIHigh	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\MusicStates\MusicMiniGameIIHigh_36EEBDE7.wem		\Interactive Music Hierarchy\MusicTest\Musictest\MusicStates\MusicMiniGameIIHigh		409982
	479198805	FriggClothes2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes2_9527B92D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes2		7140
	489292963	FriggFootsteps6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps6_48720B5D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps6		5042
	502268765	EnemyHeard4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard4		4635
	518136049	EnemySeen5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen5		6662
	519976129	EnemySeen7	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen7_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\EnemyYells\EnemySeen7		38676
	520724498	EnemyDeath2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeath2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\HandsOnFlech\EnemyDeath2		15368
	524779733	EnemyFootsteps4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps4_DA8D1992.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps4		3641
	563774782	EnemySeen2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen2		7431
	572168316	B�lgerFreja	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\B�lgerFreja_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1\SeaSound\B�lgerFreja		487060
	573200276	EnemyDeadYell4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell4		15833
	603495881	EnemyClothes1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyClothes1_B179D8F7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyClothes\EnemyClothes1		5645
	604787387	EnemyHeard9	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard9_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard9		13149
	619580296	EnemyFootsteps3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps3_1BE84786.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps3		2389
	622060039	Seagulls	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Seagulls_36EEBDE7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1\Seagulls		389505
	632877712	FriggKill1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggKill1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\FriggYells\FriggKill1		5997
	665648308	EnemyDeadYell5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell5		15348
	669205068	FriggDeath6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath6_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath6		16092
	672700403	Shrooms1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Shrooms1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Shrooms\Shrooms1		28382
	696308889	EnemyHeard3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyHeard3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Heard\EnemyHeard3		5940
	705879104	EnemyDeath3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeath3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\HandsOnFlech\EnemyDeath3		16103
	724528068	FriggClothes4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggClothes4_74481FBF.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggClothes\FriggClothes4		7787
	728989711	H�rdRegnFreja	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\H�rdRegnFreja_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience3\HeavyRain\H�rdRegnFreja		1762502
	736927849	FriggDeath4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath4		10835
	741555459	FriggFootsteps2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggFootsteps2_15A33403.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggFootstepsWalk\FriggFootsteps\FriggFootsteps2		3998
	742194177	FriggKill3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggKill3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\FriggYells\FriggKill3		7404
	751342169	EnemySeen4	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen4_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen4		8671
	773330787	EnemySeen8	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen8_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\EnemyYells\EnemySeen8		38764
	777725832	RuneStoneCall5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStoneCall5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\RuneStoneHaer\RuneStoneCall5		34615
	785538899	FriggDeath2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggDeath2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\FriggDead\FriggDeath2		10851
	791827752	Forest_Soundscape_Loop(2)	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Forest_Soundscape_Loop(2)_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1\HowlingInTrees\Forest_Soundscape_Loop(2)		1693521
	826259650	Fire	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\Fire_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Fire\Fire		318848
	840212809	EnemyDeadYell6	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell6_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell6		21345
	869868155	FriggKill2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggKill2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\FriggYells\FriggKill2		7646
	884210155	EnemyDeath1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeath1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\HandsOnFlech\EnemyDeath1		16032
	890435692	EnemyFootsteps1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyFootsteps1_D92D8320.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyFootstepsWalk\EnemyFootsteps\EnemyFootsteps1		3682
	940927498	LynLangtV�k2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\LynLangtV�k2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience1\ThunderSounds\LynLangtV�k2		160875
	943385462	RuneStone1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStone1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\RuneStones\RuneStone1		47025
	993679456	LynT�t	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\LynT�t_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience3\ThunderSound\LynT�t		147884
	1010759714	EnemyDeadYell2	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell2_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell2		10473
	1040315267	RainFreja	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RainFreja_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\Ambience\Ambience2\LightRain\RainFreja		872873
	1046823875	EnemySeen9	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen9_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\FriggDead\EnemyYells\EnemySeen9		46154
	1051500362	FriggKill5	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\FriggKill5_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\FriggYells\FriggKill5		10221
	1072003975	EnemySeen1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemySeen1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\AlertSounds_Seen\EnemySeen1		7652
	1072536413	EnemyDeadYell3	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\EnemyDeadYell3_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\EnemyDeathSound\EnemySaysSound\EnemyDeadYell3		21514
	1073576718	RuneStoneCall1	C:\Users\Dadiu student\Documents\projectstorm\ProjectStorm\ProjectStorm_WwiseProject\.cache\Mac\SFX\RuneStoneCall1_99D70269.wem		\Actor-Mixer Hierarchy\Default Work Unit\Sounds\Looping\RuneStoneHaer\RuneStoneCall1		32007

